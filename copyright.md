**Acme Corporation - CONFIDENTIAL**
 __________________
 Copyright (C) 2020 , **Acme Corporation** . All Rights Reserved
   
**NOTICE**:  
All information contained herein is, and remains  
the property of **Acme Corporation**  and its suppliers,  
if any. The intellectual and technical concepts contained  
herein are proprietary to **Acme Corporation**  and its suppliers and   
may be covered by Indian copyright Act.  
Dissemination of this information or reproduction of this material  
is strictly forbidden unless prior written permission is obtained  
from **Acme Corporation**.   
 
 Written by   
 John doe  
 <acmecorp@acme.in>, October, 2020.  

